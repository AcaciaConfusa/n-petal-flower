# N Petal Flower

 //////////////////////////////////////////////   
// N-Petal Flower  
// Hans Heidmann 2021  
// Draws a "flower" with N petals on a canvas element using javascript  
///////////////////////////////////////////////////////////////////////////  
// Variables  
var n = 7;  
var maxRadius = 250;  
var coolLines = false;  
var showCircle = true;  
var centerX = canvas.width/2;  
var centerY = canvas.height/2;  
